terraform {
  required_version = ">=1.5.5"
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.34.1"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.0.1"
    }
  }
  backend "s3" {
    key                         = "state/terraform.tfstate"
    bucket                      = "matheus-infra-tfstate"
    endpoint                    = "https://nyc3.digitaloceanspaces.com"
    region                      = "us-east-1"
    skip_region_validation      = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
  }
}
provider "digitalocean" {
  token = var.do_token
}


provider "helm" {
  kubernetes {
    config_path = "~/.kube/cluster-do"
  }
}


provider "kubernetes" {
  config_path = "~/.kube/cluster-do"
}

