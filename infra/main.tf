module "vpc" {
  source          = "./modules/vpc"
  vpc_name        = var.vpc_name
  vpc_ip_range    = var.vpc_ip_range
  vpc_description = var.vpc_description
  region          = var.region
}

<<<<<<< HEAD
=======
module "registry" {
  source        = "./modules/registry"
  registry_name = var.registry_name
  region = var.region
  depends_on = [ module.vpc ]
}



>>>>>>> refs/remotes/origin/main
module "k8s" {
  source = "./modules/cluster-k8s"
  region = var.region
  cluster_name = var.cluster_name
  cluster_version = var.cluster_version
  default_node_size = var.default_node_size
  min_nodes       = var.min_nodes
  max_nodes       = var.max_nodes
  vpc_uuid = module.vpc.vpc_uuid
  environment = var.environment
}

module "helm-charts" {
  source = "./modules/cluster-k8s/helm"
  environment = var.environment
  depends_on = [ module.k8s ]
}
module "gitlab_runner" {
  source = "./modules/gitlab-runner"
  depends_on = [ module.k8s ]
}

