variable "region" {}
variable "environment" {
  
}

variable "cluster_name" {}

variable "cluster_version" {}

variable "default_node_size" {}

variable "min_nodes" {}

variable "max_nodes" {}
variable "vpc_uuid" {
  
}
