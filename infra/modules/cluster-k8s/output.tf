output "kubeconfig" {
  value = digitalocean_kubernetes_cluster.cluster-k8s.kube_config
}