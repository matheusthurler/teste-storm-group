resource "digitalocean_kubernetes_cluster" "cluster-k8s" {
  name    = var.cluster_name
  region  = var.region
  version = var.cluster_version
  registry_integration = true
  vpc_uuid = var.vpc_uuid
  node_pool {
    name       = "${var.cluster_name}-default-pool"
    size       = var.default_node_size
    auto_scale = true
    min_nodes  = var.min_nodes
    max_nodes  = var.max_nodes
  }
}
