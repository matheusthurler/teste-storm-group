

resource "helm_release" "kube-prometheus" {
  name       = "kube-prometheus-stackr"
  create_namespace = true
  namespace  = "monitoring" # Se você deseja especificar o namespace, descomente esta linha
  version    = "56.3.0"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  # depends_on = [ kubernetes_namespace.monitoring ]
}

# resource "helm_release" "loki-logs" {
#   name       = "loki-logs"
#   repository = "https://grafana.github.io/helm-charts"
#   chart      = "loki-stack"
#   namespace = "monitoring"
#   create_namespace = true
#   set {
#     name  = "promtail.enabled"
#     value = "true"
#   }
#   set {
#     name  = "prometheus.enabled"
#     value = "true"
#   }
#   set {
#     name  = "grafana.enabled"
#     value = "true"
#   }
# }

resource "null_resource" "kube_prometheus_ingress" {
  provisioner "local-exec" {
    command = "kubectl apply -f ${path.module}/ingress-kube-prometheus.yml"
  }
  depends_on = [ helm_release.kube-prometheus]
}
