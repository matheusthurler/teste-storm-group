resource "local_file" "kubeconfig" {
  content  = digitalocean_kubernetes_cluster.cluster-k8s.kube_config[0].raw_config
  filename = pathexpand("~/.kube/cluster-do")
}

