resource "null_resource" "gitlab_runner" {
  provisioner "local-exec" {
    command = <<EOT
      helm repo add gitlab https://charts.gitlab.io
      helm repo update gitlab
      helm install --namespace gitlab --create-namespace gitlab-runner -f ${path.module}/values.yaml gitlab/gitlab-runner
    EOT
    }
}

