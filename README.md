# Teste da Storm Group - Documentação

## Instalação do doctl

Para iniciar, é necessário instalar o `doctl`, a CLI oficial da DigitalOcean.

Para instalar o `doctl`:

- **Linux/macOS:**
  
  ```bash
  curl -sL https://github.com/digitalocean/doctl/releases/download/v1.64.0/doctl-1.64.0-linux-amd64.tar.gz | tar -xzv
  sudo mv ./doctl /usr/local/bin
  ```
Login com token da DigitalOcean

- Após a instalação, faça login na DigitalOcean utilizando um token de acesso criado na plataforma:

  ```bash
  doctl auth init --access-token SEU_TOKEN_AQUI
    ```
Substitua SEU_TOKEN_AQUI pelo token de acesso gerado na sua conta da DigitalOcean.
## Criação de Access e Secret Key para a DO usando o AWS CLI

 ```bash
  sudo apt-get update
  sudo apt-get install awscli
  aws configure (informe as credenciais de access e secret geradas no menu API da digital ocean)
  ```

## Instalacao do kubectl e do terraform

```bash
#TERRAFORM
    sudo apt-get update && sudo apt-get install -y gnupg software-properties-common

    wget -O- https://apt.releases.hashicorp.com/gpg | \
    gpg --dearmor | \
    sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg > /dev/null
    gpg --no-default-keyring \
    --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
    --fingerprint
    echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \

    sudo tee /etc/apt/sources.list.d/hashicorp.list
    sudo apt update
    sudo apt-get install terraform
#KUBECTL
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
    sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
    kubectl version --client
```
## Criando a infraestrutura

- Agora com todas as dependencias instaladas, vamos dar alguns comandos do terraform para deployar o cluster kubernetes do plano free tier da Digital Ocean para novos clientes.

- Na raiz do nosso projeto temos 2 pastas, APP e INFRA, entre na pasta infra, edite o unico arquivo .tfvar.example, altere os valores das variaveis e o renomeie removendo o .example.

- A nossa pipeline como esta integrada via gitlab-runner, temos uma configuracao de tag que foi configurada ao criar um gitlab runner, com isso, o agente que foi instalado junto com terraform ira automatizar o processo de build e deploy da aaplicacao

- Na pasta infra/modules/gitlab-runner/values.yaml, edite esse arquivo com o token do gitlab runner

- Siga os comandos

```bash
    terraform init
    terraform apply var-file=development.tfvars 
    #confira os recursos a serem criados e de o comando yes
    yes
```

- Aqui vou ressaltar a configuracao do repositorio do gitlab, onde criamos um gitlab runner com helm, facilitando a integracao do resitorio com o cluster somente usando o cluster do runner sem a necessidade das chaves SSH. Vamos usar tambem a configuracao do gitlab regitry para armazenar nossa imagem buildada do docker usando kaniko e vamos salvar o kubeconfig como variavel do repositorio.

- Com todas essas configurações, foram instaladas o kube-prometheus, para analise e monitoramento do cluster e da aplicação, com as credenciais padroes de login: admin, senha: prom-operator.

- Foi criado um kubeconfig ~/.kube/cluster-do

- para testar a aplicacao, podemode usar o port-forward do servico do kubernetes e para acessar tanto nossa API quanto o grafana e prometheus, localmente.

```bash
#Expondo o grafana
kubectl port-forward svc/kube-prometheus-stack-grafana 8081:80 -n monitoring
#Expondo a aplicacao 
kubectl port-forward svc/api-teste-storm-group-api-storm-group 8000:80 -n monitoring

```
- Agora podemos acessar no nosso navegador localhost:8081 e fazer login no grafana e no localhost:800 fazer o teste da API

```bash
    # matéria 1
curl -sv localhost:8000/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"alice@example.com","comment":"first post!","content_id":1}'
curl -sv localhost:8000/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"alice@example.com","comment":"ok, now I am gonna say something more useful","content_id":1}'
curl -sv localhost:8000/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"bob@example.com","comment":"I agree","content_id":1}'

# matéria 2
curl -sv localhost:8000/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"bob@example.com","comment":"I guess this is a good thing","content_id":2}'
curl -sv localhost:8000/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"charlie@example.com","comment":"Indeed, dear Bob, I believe so as well","content_id":2}'
curl -sv localhost:8000/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"eve@example.com","comment":"Nah, you both are wrong","content_id":2}'

# listagem matéria 1
curl -sv localhost:8000/api/comment/list/1

# listagem matéria 2
curl -sv localhost:8000/api/comment/list/2
```
